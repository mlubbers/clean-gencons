# gencons

This library provides generic functions for toying with constructors.

## Licence

The package is licensed under the BSD-2-Clause license; for details, see the
[LICENSE](/LICENSE) file.
